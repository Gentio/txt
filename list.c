#include <stdlib.h>
#include <stdio.h>
#include "data.h"
int islast(struct list *tmp){
    return tmp->next == NULL;
}
int isempty(struct list *head)
{
	return head->next == NULL;
}

void list_init(struct list *head, int length)
{
    struct list *tmp;
	tmp = head;
	for (; length > 0; length--){
		tmp->x = length;
		tmp->y = 2 * length;
		tmp->next = (struct list *) malloc(sizeof(struct list));
		tmp = tmp->next;
	}
	tmp->next = NULL;
}


struct list *find(struct list *head, int x)
{
    struct list *temp;
    if (head->x == x){
        return head;
    }else {
        temp = head->next;
    }
    while(temp != NULL && temp->x != x){
        temp = temp->next;
    }
    return temp;
}


struct list *find_previous(struct list *head, int x)
{
    struct list *temp;
    temp = head;

    while (temp->next != NULL && temp->next->x != x)
    temp = temp->next;
    return temp;
}

void delete(struct list *head, int x)
{
    struct list *temp, *delete;
    temp = find_previous(head, x);
    if(!islast(temp)){
        delete = temp->next;
        temp->next = delete->next;
        free(delete);
    }
}

void print_list(struct list *head)
{
    struct list  *curt;
    curt = head;
    while(curt != NULL){
        printf("x: %d y: %d\n", curt->x, curt->y);
        curt = curt->next;
    }
}

void insert(struct list *head, int x)
{
    struct list *insert, *temp;
    temp = find_previous(head, 3);
    insert = (struct list *) malloc(sizeof(struct list));
    insert->x = x;
    insert->y = 2*x;
    insert->next = temp->next;
    temp->next = insert;
}