CFLAGS=-Wall -g


all: math.o stringbug.o scanf.o ex19

ex19: object.o

%.o: %.c
	gcc -o $*.o $<

clean:
	rm -f ex*
