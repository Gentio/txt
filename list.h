#include "data.h"

void list_init(struct list *head, int length);
int isempty(struct list *head);

struct list *find_previous(struct list *head, int x);
void delete(struct list *head, int x);
void insert(struct list *head, int x);
int islast(struct list *tmp);
void print_list(struct list *head);