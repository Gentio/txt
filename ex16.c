#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

struct person{
    char *name;
    int   age;
    int height;
    int weight;
};

struct person *person_create(char *name, int age, int height, int weight)
{
    struct person *who = malloc(sizeof(struct person));
    assert(who != NULL);

    who->name = strdup(name);
    who->age  = age;
    who->height = height;
    who->weight = weight;

    return who;
}

void person_destory(struct person *who)
{
    assert(who != NULL);

    //   free(who->name);
    free(who);
}

void person_print(struct person *who)
{
    printf("Name: %s\n", who->name);
    printf("\t Age: %d\n", who->age);
    printf("\t Height: %d\n", who->height);
    printf("\t Weight: %d\n", who->weight);
}

int main(int argc, char *argv[])
{
    // make two people structures
    struct person *joe = person_create("Joe Alex",
                                       32, 64, 140);
    struct person *frank = person_create("Frank Blank",
                                         20, 72, 180);

    // print them out and where they are in memory
    printf("Joe is at memory location %p\n", joe);
    person_print(joe);

    printf("Frank is at memory location %p\n", frank);
    person_print(frank);

    // make everyone age 20 years and print them again
    joe->age += 20;
    joe->height -= 2;
    joe->weight += 40;
    person_print(joe);

    frank->age += 20;
    frank->height += 20;
    person_print(frank);

}
